<?php
/**
 * Created by PhpStorm.
 * User: gvn
 * Date: 28.03.20
 * Time: 20:09
 */

namespace App\Client;


class Name{
    private $name;
    private $patronymic;
    private $surname;

    public function __construct($name, $patronymic, $surname){
        $this->name = $name;
        $this->patronymic = $patronymic;
        $this->surname = $surname;
    }
}