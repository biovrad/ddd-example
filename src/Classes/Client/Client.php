<?php
/**
 * Created by PhpStorm.
 * User: gvn
 * Date: 28.03.20
 * Time: 20:02
 */

namespace App\Client;


class Client{
    private $id;
    private $name;
    private $address;
    private $phone;

    public function __construct($id, $name, $address, $phone){
        $this->id = $id;
        $this->name = $name;
        $this->address = $address;

        // try{
        // if (!preg_match('/^\d+$/', $phone)) {
        // throw new Exception($phone . ' - is not valid!');
        // }
        // } catch(Exception $e){
        // echo $e->getMessage();
        // exit;
        // }
        $this->phone = $phone;
    }
}