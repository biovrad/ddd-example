<?php
/**
 * Created by PhpStorm.
 * User: gvn
 * Date: 28.03.20
 * Time: 20:09
 */

namespace App\Client;


class Phone{
    private $codeCountry;
    private $code;
    private $phone;

    public function __construct($codeCountry, $code, $phone){
        $this->codeCountry = $codeCountry;
        $this->code = $code;
        $this->phone = $phone;
    }
}