<?php

namespace App\Client;


class Address{
    private $country;
    private $city;
    private $zip;
    private $lines;

    public function __construct($country, $city, $zip, $lines){
        $this->country = $country;
        $this->city = $city;
        $this->zip = $zip;
        $this->lines = $lines;
    }
}
