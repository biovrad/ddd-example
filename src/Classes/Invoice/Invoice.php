<?php

namespace App\Invoice;


use App\Client\Client;
use App\Item\LineItem;
use App\Status\Status;

class Invoice{
    private $id;
    private $client;
    private $clientItem = [];
    private $status;
    private $lineItmes;

    public function __construct($id, Client $client){
        $this->id = $id;
        $this->client = $client;
    }

    public function changeStatus(Status $status)
    {
        $this->status = $status;
        $this->status->ensureCanBeChangedTo($status);
        $this->status = $status;
    }

    public function addLineItem(LineItem $lineItem)
    {
        $this->status->ensureAllowsModification();
//        $this->lineItmes[] = $lineItem;
//        array_push($this->lineItmes, $lineItem);
//        $this->lineItmes->push($lineItem);
    }

//    public function removeLineItem(LineItem $lineItem)
//    {
//        $this->status->ensureAllowsModification();
//        // ...
//    }

    public function getId() {}
    public function getClient() {}
    public function getLineItems($lineItmes) {
        return $this->lineItmes;
    }
    public function setLineItems($clientItem) {
        $this->clientItem = $clientItem;
    }
    public function getStatus() {}
    public function setStatus($status){
        $this->status = $status;
    }
}