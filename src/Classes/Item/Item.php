<?php
/**
 * Created by PhpStorm.
 * User: gvn
 * Date: 28.03.20
 * Time: 20:05
 */

namespace App\Item;


class Item {
    private $id;
    private $name;
    private $description;
    private $price;

    public function __construct($id, $name, $price){
        $this->id = $id;
        $this->name = $name;
        $this->price = $price;
    }
    public function setDescription($description){
        return $this->description = $description;
    }
}