<?php

namespace App\Status;


use App\Exception\ModificationProhibitedException;
use App\Exception\WrongStatusChangeDirectionException;

abstract class Status
{
    /**
     * @property array Class names of next possible statuses
     */
    protected $next = [];

    public function ensureCanBeChangedTo(self $status)
    {
        if (!$this->canBeChangedTo($status)) {
            throw new WrongStatusChangeDirectionException('error - Be Changed');
        }
    }

    public function ensureAllowsModification()
    {
        if (!$this->allowsModification()) {
            throw new ModificationProhibitedException('error - Allows Modification');
        }
    }

    public function canBeChangedTo(self $status)
    {
        $className = get_class($status);
        return in_array($className, $this->next, true);
    }

    public function allowsModification()
    {
        return true;
    }
}
