<?php

namespace App\Status;


class NewStatus extends Status{
    protected $next = [ProcessingStatus::class, RejectedStatus::class, NewStatus::class];
}