<?php

namespace App\Exception;

use Exception;

class WrongStatusChangeDirectionException extends Exception
{
    public function __construct($message, Exception $previous = null) {
        parent::__construct($message, $previous);
    }

    public function __toString() {
        return '--#--'.__CLASS__ . ": {$this->message}\n";
    }
}
