<?php

namespace App\Order;


use App\Client\Address;
use App\Client\Client;
use App\Client\Name;
use App\Client\Phone;
use App\Helper\Many;
use App\Helper\UuId;
use App\Invoice\Invoice;
use App\Item\Item;
use App\Item\LineItem;
use App\Status\NewStatus;

class Order
{
    public function addOrder() {

        $item = new Item(UuId::id(), 'test', Many::usa(777));
        $item->setDescription('Description test');
        $client = new Client(
            UUID::id(),
            new Name('Фамилия', 'Имя', 'Отчество'),
            new Address('Украина', 'Киев', '01001', ['ул. Кодеров, д. 45']),
            new Phone('380', '44', '1234567')
        );

        $invoice = new Invoice(UUID::id(), $client);
        $invoice->setStatus('new');
        $invoice->setLineItems([new LineItem($item, 2)]);

        $newItem = new Item(UuId::id(), 'test-new', Many::usa(9999));
        $anotherLine = new LineItem($newItem, 9);
        $invoice->setLineItems([$anotherLine]);
        $newStatus = new NewStatus();
        $invoice->changeStatus($newStatus);

        $newItem2 = new Item(UuId::id(), 'test-new-2', Many::usa(10000));
        $anotherLine2 = new LineItem($newItem2, 10);
        dump($anotherLine2);
        $invoice->addLineItem($anotherLine2);

        dump($invoice);
    }

}